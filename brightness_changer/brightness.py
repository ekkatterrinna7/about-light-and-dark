def darken(color, percent):
    """
    Затемнение цвета.
    """
    result = ''
    bright = 1 - percent / 100
    r = int(color[0] + color[1], 16)
    g = int(color[2] + color[3], 16)
    b = int(color[4] + color[5], 16)
    r = hex(round(r * bright))[2:]
    g = hex(round(g * bright))[2:]
    b = hex(round(b * bright))[2:]
    for i in r, g, b:
        if len(i) == 1:
            i = '0' + i
        result += i.upper()
    return result


def lighten(color, percent):
    """
    Осветление цвета.
    """
    result = ''
    bright = percent / 100
    r = int(color[0] + color[1], 16)
    g = int(color[2] + color[3], 16)
    b = int(color[4] + color[5], 16)
    r = hex(round(r + (255 * bright)))[2:]
    g = hex(round(g + (255 * bright)))[2:]
    b = hex(round(b + (255 * bright)))[2:]
    for i in r, g, b:
        if len(i) == 1:
            i = '0' + i
        if len(i) > 2:
            i = 'ff'
        result += i.upper()
    return result
